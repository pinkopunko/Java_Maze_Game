import java.util.ArrayList;
import java.util.Collections;

/**
 * 
 */

/**
 * @author zaisi
 *
 */
public class Maze {
	private int x;
	private int y;
	private ArrayList<Cell> allCells;
	private int mazeComplexity;
	
	public Maze(int mazeWidth, int mazeHeight, int mazeComplexity) {
		this.mazeComplexity = mazeComplexity;
		x = mazeWidth;
		y = mazeHeight;
		allCells = new ArrayList<Cell>();
		for (int j = 0; j < y; j++) {
			for (int i = 0; i < x; i++) {
				Cell cell = new Cell(i, j);
				allCells.add(cell);
			}
		}
		
		if(this.mazeComplexity == 1){
			for (Cell cell : allCells) {
				int cellX = cell.getX();
				int cellY = cell.getY();
				if (cellY != 0) cell.setCellInDirection("up", getCell(cellX, cellY - 1));
				if (cellY != y - 1) cell.setCellInDirection("down", getCell(cellX, cellY + 1));
				if (cellX != 0) cell.setCellInDirection("left", getCell(cellX - 1, cellY));
				if (cellX != x - 1) cell.setCellInDirection("right", getCell(cellX + 1, cellY));
			}
			
			// Prim to dig path: recursive
			Cell curr = getCell(1, 1);
			curr.setWall(false);
			dfs(curr);
	
			// take down the wall at (0, 1) since the players start at this position
			getCell(0, 1).setWall(false);
			// set the bottom right corner as goal
			getCell(mazeWidth - 2, mazeHeight - 2).setGoal(true);
			
			
		}
			
		else if(this.mazeComplexity == 2 || this.mazeComplexity == 3){
			for (Cell cell : allCells) {
				int cellX = cell.getX();
				int cellY = cell.getY();
				if (cellY != 0) cell.setCellInDirection("up", getCell(cellX, cellY - 1));
				if (cellY != y - 1) cell.setCellInDirection("down", getCell(cellX, cellY + 1));
				if (cellX != 0) cell.setCellInDirection("left", getCell(cellX - 1, cellY));
				if (cellX != x - 1) cell.setCellInDirection("right", getCell(cellX + 1, cellY));
			}
			
			// DFS to dig path: recursive
			Cell curr = getCell(1, 1);
			curr.setWall(false);
			prim(curr);

			// take down the wall at (0, 1) since the players start at this position
			getCell(0, 1).setWall(false);
			// set the bottom right corner as goal
			getCell(mazeWidth - 2, mazeHeight - 2).setGoal(true);
			
		}
	}		
	
	public Cell getCell (int cellX, int cellY) {
		return allCells.get(cellY * x + cellX);
	}
	
	private void dfs(Cell curr) {
		ArrayList<String> directions = new ArrayList<String>();
		directions = curr.getValidDirections();
		Collections.shuffle(directions);
		for (int i = 0; i < directions.size(); i++) {
			Cell oneCellAway = curr.getCell(directions.get(i));
			if (oneCellAway == null) continue;
			else {
				Cell twoCellsAway = oneCellAway.getCell(directions.get(i));
				if (twoCellsAway == null) continue;
				else {
					if (twoCellsAway.isWall()) {
						oneCellAway.setWall(false);
						twoCellsAway.setWall(false);
						dfs(twoCellsAway);
					}
				}
			}
		}
	}
	
	/*
	 * PRIM ALGORITHM RIGHT HERE!!!!!
	 * PRIM ALGORITHM RIGHT HERE!!!!!
	 * PRIM ALGORITHM RIGHT HERE!!!!!
	 */
	private void prim(Cell curr) {
		ArrayList<String> directions = new ArrayList<String>();
		ArrayList<Cell> frontier = new ArrayList<Cell>();
		directions.add("right");
		Cell oneCellAway = curr.getCell(directions.get(0));
		Cell twoCellAway = oneCellAway.getCell(directions.get(0));
		frontier.add(twoCellAway);
		directions.add("down");
		oneCellAway = curr.getCell(directions.get(1));
		twoCellAway = oneCellAway.getCell(directions.get(1));
		frontier.add(twoCellAway);
		int connected = 0;
		while (!frontier.isEmpty()){
			Collections.shuffle(frontier);
			Cell frontier1 = frontier.get(0);
			frontier1.setWall(false);
			directions = frontier1.getValidDirections();
			Collections.shuffle(directions);
			for (int i = 0; i < directions.size(); i++) {
				Cell neighbourOne = frontier1.getCell(directions.get(i));
				if (neighbourOne == null || !neighbourOne.isWall()) continue;
				else {
					Cell neighbourTwo = neighbourOne.getCell(directions.get(i));
					if (neighbourTwo == null) continue;
					else if (neighbourTwo.isWall()) {
						if (frontier.contains(neighbourTwo)) continue;
						frontier.add(neighbourTwo);
						continue;
					}
					else {
						if (!neighbourTwo.isWall()) {
							if (connected == 0){
								neighbourOne.setWall(false);
								neighbourTwo.setWall(false);
								frontier.remove(0);
								connected = 1;
								continue;
							}
						}
					}
				}
			}
			connected = 0;
		}	
	}	
}