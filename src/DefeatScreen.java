import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DefeatScreen extends JPanel{
	Image defeat;
	private JLabel defeatLabel;
	private JButton tryAgain;
	private JButton mainMenu;
	
	
	public DefeatScreen() {
		setLayout(null);
		createBackgroundImage();
		createDefeatLabel();
		createButtons();
	}
	
	
	private void createBackgroundImage() {
		try {
			defeat = ImageIO.read(new File("defeat.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void createButtons() {
		mainMenu = new JButton("Main Menu");
		mainMenu.setBounds(350, 300, 200, 50);
		tryAgain = new JButton("Try Again");
		tryAgain.setBounds(350, 250, 200, 50);
		
		this.add(mainMenu);
		this.add(tryAgain);
		
	}
	
	private void createDefeatLabel() {
		defeatLabel = new JLabel();
		defeatLabel.setBounds(270, 100, 1000, 100);
		defeatLabel.setText("IT'S OK. WE KNOW YOU'VE TRIED.");
		defeatLabel.setFont(new Font("Courier New", Font.BOLD, 25));
		defeatLabel.setForeground(Color.WHITE);
		this.add(defeatLabel);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
	   super.paintComponent(g); 
	   if (defeat != null)
	     g.drawImage(defeat, 0, 0, this.getWidth(),this.getHeight(), this);
	}
	
	public void addActionListener (GUI gui) {
		mainMenu.addActionListener(gui);
		tryAgain.addActionListener(gui);
	}
}
