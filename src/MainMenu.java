import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MainMenu extends JPanel{
	private Image image;
	private JLabel titleLabel;
	private JButton startGame;
	private JButton options;
	private JButton quitGame;
	
	public MainMenu() {
		setLayout(null);
		createBackgroundImage();
		createTitle();
		createButtons();
	}
	
	private void createBackgroundImage() {
		try {
			image = ImageIO.read(new File("background.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void createTitle() {
		titleLabel = new JLabel();
		titleLabel.setBounds(80, 250, 400, 100);
		titleLabel.setText("Pet Adventure");
		titleLabel.setFont(new Font("Courier New", Font.BOLD, 48));
		titleLabel.setForeground(Color.WHITE);
		this.add(titleLabel);
	}
	
	private void createButtons() {
		ImageIcon startIcon = new ImageIcon("player1down.png");
		startGame = new JButton("Start Game", startIcon);
		startGame.setOpaque(false);
		startGame.setContentAreaFilled(false);
		startGame.setBorderPainted(false);
		startGame.setFocusPainted(false);
		startGame.setFont(new Font("Serif", Font.PLAIN, 18));
		startGame.setForeground(Color.WHITE);
		startGame.setBounds(125, 350, 200, 50);
		ImageIcon optionIcon = new ImageIcon("player1left.png");
		options = new JButton("Options", optionIcon);
		options.setOpaque(false);
		options.setContentAreaFilled(false);
		options.setBorderPainted(false);
		options.setFocusPainted(false);
		options.setFont(new Font("Serif", Font.PLAIN, 18));
		options.setForeground(Color.WHITE);
		options.setBounds(125, 400, 200, 50);
		ImageIcon quitIcon = new ImageIcon("player1up.png");
		quitGame = new JButton("Quit", quitIcon);
		quitGame.setOpaque(false);
		quitGame.setContentAreaFilled(false);
		quitGame.setBorderPainted(false);
		quitGame.setFocusPainted(false);
		quitGame.setFont(new Font("Serif", Font.PLAIN, 18));
		quitGame.setForeground(Color.WHITE);
		quitGame.setBounds(125, 450, 200, 50);
		
		this.add(startGame);
		this.add(options);
		this.add(quitGame);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
	   super.paintComponent(g); 
	   if (image != null)
	     g.drawImage(image, 0,0,this.getWidth(),this.getHeight(),this);
	}
	
	
	public void addActionListener (GUI gui) {
		startGame.addActionListener(gui);
		options.addActionListener(gui);
		quitGame.addActionListener(gui);
	}
}
