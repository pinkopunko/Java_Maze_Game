import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class GUI extends JFrame implements ActionListener, KeyListener{
	private JPanel basePanel;
	private Board board;
	private Options options;
	private VictoryScreen victoryScreen;
	private DefeatScreen defeatScreen;
	
	
	public GUI() {
		initUI();
	}
	
	private void initUI() {
		
		createMenuBar();
		
		basePanel = new JPanel(new CardLayout());
		
		MainMenu mainMenu = new MainMenu();
		basePanel.add(mainMenu, "MAINMENU");
		board = new Board(this);
		basePanel.add(board, "START");
		options = new Options();
		basePanel.add(options, "OPTIONS");
		victoryScreen = new VictoryScreen();
		basePanel.add(victoryScreen, "VICTORY");
		defeatScreen = new DefeatScreen();
		basePanel.add(defeatScreen, "DEFEAT");
		
		this.add(basePanel, BorderLayout.CENTER);
		
		mainMenu.addActionListener(this);
		options.addActionListener(this);
		victoryScreen.addActionListener(this);
		defeatScreen.addActionListener(this);
		board.addKeyListener(this);
		
		this.setResizable(true);
		this.setSize(1240, 900);
		this.setTitle("Pet Adventures");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	
	}
	
	private void createMenuBar() {
		JMenuBar menubar = new JMenuBar();
		menubar.setPreferredSize(new Dimension(-1, 25));
		JMenu menu = new JMenu("Menu");
		menu.setFont(new Font("Serif", Font.PLAIN, 18));
		JMenuItem newGame = new JMenuItem("New Game");
		newGame.setFont(new Font("Serif", Font.PLAIN, 18));
		newGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				CardLayout cardLayout = (CardLayout) basePanel.getLayout();
				board.initBoard();
				cardLayout.show(basePanel, "START");
			}
		});
		JMenuItem mainMenu = new JMenuItem("Main Menu");
		mainMenu.setFont(new Font("Serif", Font.PLAIN, 18));
		mainMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				board.stopTimer();
				CardLayout cardLayout = (CardLayout) basePanel.getLayout();
				cardLayout.show(basePanel, "MAINMENU");
			}
		});
		JMenuItem quit = new JMenuItem("Quit");
		quit.setFont(new Font("Serif", Font.PLAIN, 18));
		quit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				System.exit(0);
			}
		});
		menu.add(newGame);
		menu.add(mainMenu);
		menu.add(quit);
		menubar.add(menu);
		setJMenuBar(menubar);
	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		CardLayout cardLayout = (CardLayout) basePanel.getLayout();
		if (e.getActionCommand().equals("Start Game") ||
			e.getActionCommand().equals("Try Again") ||
			e.getActionCommand().equals("Play Again")) {
			board.initBoard();
			cardLayout.show(basePanel, "START");
		} else if (e.getActionCommand().equals("Cancel") || 
				   e.getActionCommand().equals("Main Menu")) {
			cardLayout.show(basePanel, "MAINMENU");
		} else if (e.getActionCommand().equals("Options")) {
			cardLayout.show(basePanel, "OPTIONS");
		} else if (e.getActionCommand().equals("Quit")) {
			System.exit(0);
		} else if (e.getActionCommand().equals("Next Level")){
			if(board.getMazeComplexity() == 1) {
				board.setMazeComplexity(2);
				options.setMazeComplexity(2);
			} else if (!board.isNightMode()) {
				board.setNightMode(true);
				options.turnOnNightMode(true);
			} else if (board.getPredatorStatus() < 4) {
				int newValue = board.getPredatorStatus() + 1;
				board.setPredatorStatus(newValue);
				options.predatorStatusUp();
			}
			board.initBoard();
			cardLayout.show(basePanel, "START");
		} else if (e.getActionCommand().equals("Save")){
			cardLayout.show(basePanel, "MAINMENU");
			this.setSize(options.getScreenWidth(),options.getScreenHeight());
			board.setNumPlayers(options.getNumPlayers());
			board.setMazeComplexity(options.getMazeComplexity());
			board.setPredatorStatus(options.getPredatorStatus());
			board.setNightMode(options.isNightMode());	
		}
	}
	
	public void keyPressed(KeyEvent e){
		CardLayout cardLayout = (CardLayout) basePanel.getLayout();
		int keycode = e.getKeyCode();
		Player player1 = board.getPlayer(1);
		Player player2 = board.getPlayer(2);
		if (!player1.captured()) {
			if ((keycode == KeyEvent.VK_LEFT && player1.getLocation().getCell("left").isGoal()) ||
				(keycode == KeyEvent.VK_RIGHT && player1.getLocation().getCell("right").isGoal()) ||
				(keycode == KeyEvent.VK_UP && player1.getLocation().getCell("up").isGoal()) ||
				(keycode == KeyEvent.VK_DOWN && player1.getLocation().getCell("down").isGoal())) {
				board.stopTimer();
				int timeUsed = board.getSecondCounter();
				if (player2 == null) victoryScreen.setWinner(0);
				else victoryScreen.setWinner(1);
				victoryScreen.updateVictoryScreen(timeUsed);
				cardLayout.show(basePanel, "VICTORY");
			}
			if ((keycode == KeyEvent.VK_LEFT && player1.getLocation().getCell("left").predator()) ||
				(keycode == KeyEvent.VK_RIGHT && player1.getLocation().getCell("right").predator()) ||
				(keycode == KeyEvent.VK_UP && player1.getLocation().getCell("up").predator()) ||
				(keycode == KeyEvent.VK_DOWN && player1.getLocation().getCell("down").predator())) {
				removeKeyListener(player1);
	        	player1.getLocation().setPlayer(1, false);
	        	player1.setCaptured(true);
			}
		}
		if (player2 != null && !player2.captured()) {
			if ((keycode == KeyEvent.VK_A && player2.getLocation().getCell("left").isGoal()) ||
				(keycode == KeyEvent.VK_D && player2.getLocation().getCell("right").isGoal()) ||
				(keycode == KeyEvent.VK_W && player2.getLocation().getCell("up").isGoal()) ||
				(keycode == KeyEvent.VK_S && player2.getLocation().getCell("down").isGoal())) {
				board.stopTimer();
				int timeUsed = board.getSecondCounter();
				victoryScreen.setWinner(2);
				victoryScreen.updateVictoryScreen(timeUsed);
				cardLayout.show(basePanel, "VICTORY");
			}
			if ((keycode == KeyEvent.VK_A && player2.getLocation().getCell("left").predator()) ||
				(keycode == KeyEvent.VK_D && player2.getLocation().getCell("right").predator()) ||
				(keycode == KeyEvent.VK_W && player2.getLocation().getCell("up").predator()) ||
				(keycode == KeyEvent.VK_S && player2.getLocation().getCell("down").predator())) {
				removeKeyListener(player2);
	        	player2.getLocation().setPlayer(2, false);
	        	player2.setCaptured(true);
			}
		}
		if((player1.captured() == true && player2 == null) ||
           (player1.captured() == true && player2 != null && player2.captured())){
        	board.stopTimer();
        	showDefeatScreen();
        }
		
	}
	
	public void showDefeatScreen() {
		CardLayout cardLayout = (CardLayout) basePanel.getLayout();
		cardLayout.show(basePanel, "DEFEAT");
	}

	public static void main (String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				GUI gui = new GUI();
				gui.setVisible(true);
			}
		});
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
