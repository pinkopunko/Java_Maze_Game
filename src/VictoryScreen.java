

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class VictoryScreen extends JPanel{
	Image victory;
	Image puppy_victory;
	Image kitten_victory;
	Image single_victory;
	private JLabel victoryLabel;
	private JLabel timeLabel;
	private JButton nextLevel;
	private JButton mainMenu;
	private int winner; // 0-single player; 1-puppy; 2-kitten
	
	
	public VictoryScreen() {
		winner = 0;
		setLayout(null);
		createBackgroundImage();
		createVictoryLabel();
		createTimeLabel();
		createButtons();
	}
	
	public void updateVictoryScreen(int timeUsed) {
		updateBackgroundImage();
		updateVictoryLabel();
		updateTimeLabel(timeUsed);
	}
	
	private void createBackgroundImage() {
		try {
			puppy_victory = ImageIO.read(new File("puppy_victory.jpg"));
			kitten_victory = ImageIO.read(new File("kitten_victory.jpg"));
			single_victory = ImageIO.read(new File("single_victory.jpg"));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.victory = single_victory;
	}
	
	private void updateBackgroundImage() {
		if (winner == 1) this.victory = puppy_victory;
		else if (winner == 2) this.victory = kitten_victory;
		else this.victory = single_victory;
	}
	
	private void createButtons() {
		mainMenu = new JButton("Main Menu");
		mainMenu.setBounds(350, 300, 200, 50);
		nextLevel = new JButton("Next Level");
		nextLevel.setBounds(350, 250, 200, 50);
		
		this.add(mainMenu);
		this.add(nextLevel);
		
	}
	
	private void createVictoryLabel() {
		victoryLabel = new JLabel();
		victoryLabel.setBounds(270, 100, 1000, 100);
		victoryLabel.setText("VICTORY");
		victoryLabel.setFont(new Font("Courier New", Font.BOLD, 100));
		victoryLabel.setForeground(Color.WHITE);
		this.add(victoryLabel);
	}
	
	private void createTimeLabel() {
		timeLabel = new JLabel();
		timeLabel.setBounds(270, 150, 1000, 100);
		timeLabel.setText("Time Used:");
		timeLabel.setFont(new Font("Courier New", Font.PLAIN, 25));
		timeLabel.setForeground(Color.WHITE);
		this.add(timeLabel);
	}
	
	private void updateVictoryLabel() {
		if (winner == 0) victoryLabel.setText("YOU WIN!");
		else if (winner == 1) victoryLabel.setText("PUPPY WINS!");
		else if (winner == 2) victoryLabel.setText("KITTEN WINS!");

	}
	
	private void updateTimeLabel(int timeUsed) {
		int hr = 0;
    	int min = 0;
    	int sec = timeUsed;
    	while (sec >= 3600) {
    		hr++;
    		sec-=3600;
    	}
    	while (sec >= 60) {
    		min++;
    		sec-=60;
    	}
		timeLabel.setText("Time used: "+hr+" hr "+min+" min "+sec+" sec");
	}
	
	@Override
	protected void paintComponent(Graphics g) {
	   super.paintComponent(g); 
	   if (victory != null)
	     g.drawImage(victory, 0,0,this.getWidth(),this.getHeight(),this);
	}
	
	public void addActionListener (GUI gui) {
		mainMenu.addActionListener(gui);
		nextLevel.addActionListener(gui);
	}
	
	public void setWinner(int winner) {
		this.winner = winner;
	}
}

