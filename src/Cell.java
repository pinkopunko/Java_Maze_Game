import java.util.ArrayList;
import java.util.HashMap;

/**
 * 
 */

/**
 * @author zaisi
 *
 */
public class Cell {
	private int x;
	private int y;
	private boolean isWall;
	private boolean isGoal;
	private HashMap<String, Cell> neighbourCells;
	private boolean player1;
	private boolean player2;
	private boolean predator;
	private int distFromLight;
	
	public Cell(int x, int y) {
		this.x = x;
		this.y = y;
		this.isWall = true;
		this.isGoal = false;
		this.neighbourCells = new HashMap<String, Cell>();
		neighbourCells.put("up", null);
		neighbourCells.put("down", null);
		neighbourCells.put("left", null);
		neighbourCells.put("right", null);
		this.player1 = false;
		this.player2 = false;
		this.predator = false;
		this.setDistFromLight(0);
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public boolean isWall() {
		return isWall;
	}
	
	public boolean isGoal() {
		return isGoal;
	}
	
	public boolean player(int ref) {
		if (ref == 1) return player1;
		else if (ref == 2) return player2;
		else return false;
	}
	
	public boolean predator() {
		return predator;
	}
	
	public void setWall(boolean tf) {
		isWall = tf;
	}
	
	public void setGoal(boolean tf) {
		isGoal = tf;
	}
	
	public void setPlayer(int ref, boolean tf) {
		if (ref == 1) player1 = tf;
		else if (ref == 2) player2 = tf;
	}
	
	public void setPredator(boolean tf) {
		predator = tf;
	}
	
	// given a direction, return a cell. 
	// could return null if neighbour in that direction does not exist
	public Cell getCell(String direction) {
		return neighbourCells.get(direction);
	}
	
	// return a list of directions that refers to a non-null Cell
	public ArrayList<String> getValidDirections() {
		ArrayList<String> directions = new ArrayList<String>();
		for (String direction : neighbourCells.keySet()) {
			if (getCell(direction) != null) directions.add(direction);
		}
		return directions;
	}
	
	public ArrayList<Cell> getNeighbourPaths() {
		ArrayList<Cell> neighbourPaths = new ArrayList<Cell>();
		for (String direction : getValidDirections()) {
			Cell cell = getCell(direction);
			if (!cell.isWall()) neighbourPaths.add(cell);
		}
		return neighbourPaths;
	}
	
	public HashMap<String, Cell> getNeighbourCells() {
		return neighbourCells;
	}
	
	// given a direction and a cell, updates the hashmap of neighbourCells
	public void setCellInDirection(String direction, Cell cell) {
		neighbourCells.put(direction, cell);
	}

	public int getDistFromLight() {
		return distFromLight;
	}

	public void setDistFromLight(int distFromLight) {
		this.distFromLight = distFromLight;
	}
	
	
}