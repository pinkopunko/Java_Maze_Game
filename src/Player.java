

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * 
 */

/**
 * @author zaisi
 *
 */
public class Player extends KeyAdapter {
	private int ref; //1 for puppy, 2 for kitten
	private boolean captured;
	private Cell location;
	private String facing;
	
	public Player(Maze maze, int ref, int x, int y) {
		this.ref = ref;
		this.captured = false;
		location = maze.getCell(x, y);
		location.setPlayer(ref, true);
		facing = "down";
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		int keycode = e.getKeyCode();
		if (ref == 1 && !captured) {
			switch (keycode) {
			case KeyEvent.VK_LEFT:
				move("left");
				break;
			case KeyEvent.VK_RIGHT:
				move("right");
				break;
			case KeyEvent.VK_UP:
				move("up");
				break;
			case KeyEvent.VK_DOWN:
				move("down");
				break;		
			}
		} else if (ref == 2 && !captured) {
			switch (keycode) {
			case KeyEvent.VK_A:
				move("left");
				break;
			case KeyEvent.VK_D:
				move("right");
				break;
			case KeyEvent.VK_W:
				move("up");
				break;
			case KeyEvent.VK_S:
				move("down");
				break;				
			}
		}
	}
		
		private boolean move(String direction) {
			facing = direction;
			Cell cell = location.getCell(direction);
			if (cell == null) return false;
			else if (cell.isWall()) return false;
			else {
				location.setPlayer(ref, false);
				location = cell;
				location.setPlayer(ref, true);
				return true;
			}	
		}
		
		public boolean captured() {
			return captured;
		}
		
		public Cell getLocation () {
			return location;
		}
		
		public String getFacing() {
			return facing;
		}
		
		public void setCaptured(boolean captured) {
			this.captured = captured;
		}
}

