

/**
 * 
 */

/**
 * @author zaisi
 *
 */
public class Predator {
	private final int MazeHeight = 21;
	private final int MazeWidth = 31;
	private Cell location;
	private String facing;
	private Player player1;
	private Player player2;
	
	public Predator(Maze maze, int x, int y, Player p1, Player p2) {
		location = maze.getCell(x, y);
		location.setPredator(true);
		facing = "up";
		player1 = p1;
		player2 = p2;
	}
	
	public Cell getLocation() {
		return location;
	}
	
	public String getFacing() {
		return facing;
	}
	
	public void move() {
		
		Cell nextMove = location;
		String moveTowards = facing;
		
		int p1x = 0;
		int p1y = 0;
		int p2x = 0;
		int p2y = 0;
		
		if (!player1.captured()) {
			Cell p1pos = player1.getLocation();
			if (location == p1pos) return;
			p1x = p1pos.getX();
			p1y = p1pos.getY();
		}
		if (player2 != null && !player2.captured()) {
			Cell p2pos = player2.getLocation();
			if (location == p2pos) return;
			p2x = p2pos.getX();
			p2y = p2pos.getY();
		}
		int d = MazeWidth * MazeWidth + MazeHeight * MazeHeight;
		for (String direction : location.getValidDirections()) {
			Cell cell = location.getCell(direction);
			int cellx = cell.getX();
			int celly = cell.getY();
			int d1 = 0;
			int d2 = 0;
			if (!player1.captured()) d1 = (p1x-cellx)*(p1x-cellx) + (p1y-celly)*(p1y-celly);
			if (player2 != null && !player2.captured()) d2 = (p2x-cellx)*(p2x-cellx) + (p2y-celly)*(p2y-celly);
			if (!player1.captured() && d1 < d) {
				d = d1;
				nextMove = cell;
				moveTowards = direction;
			}
			if (player2 != null && !player2.captured() && d2 < d) {
				d = d2;
				nextMove = cell;
				moveTowards = direction;
			}
		}
		this.location.setPredator(false);
		this.location = nextMove;
		this.location.setPredator(true);
		this.facing = moveTowards;
	}
}