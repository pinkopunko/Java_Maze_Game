import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.util.Hashtable;

public class Options extends JPanel{
	private Image image;
	private JButton mainMenu;
	private JButton mainMenuUpdate;
	private JPanel numPlayersOptions;
	private JRadioButton onePlayer;
	private JRadioButton twoPlayers;
	private JPanel mazeComplexityOptions;
	private JRadioButton simple;
	private JRadioButton complex;
	private JPanel predatorOptions;
	private JSlider predatorStatusSlider;
	private JPanel nightModeSwitch;
	private JRadioButton nightModeOn;
	private JRadioButton nightModeOff;
	private JPanel screenSize;
	private JSlider screenSizeSlider;
	
	
	private int numPlayers;
	private int mazeComplexity;
	private int predatorStatus;
	private boolean isNightMode;
	private int screenWidth;
	private int screenHeight;
	

	
	public Options() {
		this.numPlayers = 1;
		this.mazeComplexity = 1;
		this.predatorStatus = 0;
		this.isNightMode = false;
		setLayout(null);
		createBackgroundImage();
		createNumPlayersOptions();
		createMazeComplexityOptions();
		createPredatorOptions();
		createNightModeOptions();
		createScreenSizeSlider();
		createSaveAndCancel();
	}
	
	private void createBackgroundImage() {
		try {
			image = ImageIO.read(new File("optionsImage.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void createNumPlayersOptions() {
		numPlayersOptions = new JPanel();
		JLabel numPlayersLabel = new JLabel("No. of players:");
		onePlayer = new JRadioButton("1");
		twoPlayers = new JRadioButton("2");
		
		numPlayersLabel.setFont(new Font("Serif", Font.PLAIN, 18));
		numPlayersLabel.setForeground(Color.WHITE);
		onePlayer.setFont(new Font("Serif", Font.PLAIN, 18));
		onePlayer.setForeground(Color.WHITE);
		onePlayer.setOpaque(false);
		onePlayer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				numPlayers = 1;
			}
		});
		
		twoPlayers.setFont(new Font("Serif", Font.PLAIN, 18));
		twoPlayers.setForeground(Color.WHITE);
		twoPlayers.setOpaque(false);
		twoPlayers.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				numPlayers = 2;
			}
		});
		
		ButtonGroup numPlayersGroup = new ButtonGroup();
		numPlayersGroup.add(onePlayer);
		numPlayersGroup.add(twoPlayers);
		
		numPlayersOptions.add(numPlayersLabel);
		numPlayersOptions.add(onePlayer);
		numPlayersOptions.add(twoPlayers);
		
		onePlayer.setSelected(true); //default
		
		numPlayersOptions.setOpaque(false);
		numPlayersOptions.setBounds(20, 100, 350, 40);
		
		this.add(numPlayersOptions);
		
	}
	
	
	private void createMazeComplexityOptions() {
		mazeComplexityOptions = new JPanel();
		JLabel mazeComplexityLabel = new JLabel("Maze Complexity:");
		simple = new JRadioButton("Simple");
		complex = new JRadioButton("Complex");
		
		mazeComplexityLabel.setFont(new Font("Serif", Font.PLAIN, 18));
		mazeComplexityLabel.setForeground(Color.WHITE);
		simple.setFont(new Font("Serif", Font.PLAIN, 18));
		simple.setForeground(Color.WHITE);
		simple.setOpaque(false);
		simple.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mazeComplexity = 1;
			}
		});
		
		complex.setFont(new Font("Serif", Font.PLAIN, 18));
		complex.setForeground(Color.WHITE);
		complex.setOpaque(false);
		complex.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mazeComplexity = 2;
			}
		});
		
		ButtonGroup mazeComplexityGroup = new ButtonGroup();
		mazeComplexityGroup.add(simple);
		mazeComplexityGroup.add(complex);

		mazeComplexityOptions.add(mazeComplexityLabel);
		mazeComplexityOptions.add(simple);
		mazeComplexityOptions.add(complex);
		
		simple.setSelected(true); // default
		
		mazeComplexityOptions.setOpaque(false);
		mazeComplexityOptions.setBounds(20, 150, 500, 40);
		
		this.add(mazeComplexityOptions);
		
	}
	
	private void createPredatorOptions() {
		
		this.predatorOptions = new JPanel();
		JLabel predatorStatusLabel = new JLabel("<html>Predator Status:</html>");
		predatorStatusLabel.setFont(new Font("Serif", Font.PLAIN, 18));
		predatorStatusLabel.setForeground(Color.WHITE);
		
		predatorOptions.add(predatorStatusLabel);
		
		predatorStatusSlider = new JSlider (0,4,0);
		predatorStatusSlider.setOpaque(false);
		predatorStatusSlider.setSnapToTicks(true);
		predatorStatusSlider.setMajorTickSpacing(1);
		predatorStatusSlider.setPaintTicks(true);
		predatorStatusSlider.setPaintLabels(true);
		predatorStatusSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (e.getSource() == predatorStatusSlider){
					predatorStatus = predatorStatusSlider.getValue();
				}			
			}
		});
		
		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		String label1 = "<html><font color = white>off</html>";
		String label2 = "<html><font color = white>slow</html>";
		String label5 = "<html><font color = white>fast</html>";
		labelTable.put(new Integer(0), new JLabel(label1));
		labelTable.put(new Integer(1), new JLabel(label2));
		labelTable.put(new Integer(4), new JLabel(label5));
		
		predatorStatusSlider.setLabelTable(labelTable);
		predatorStatusSlider.setPaintLabels(true);
		
		predatorOptions.add(predatorStatusSlider);
		predatorOptions.setOpaque(false);
		predatorOptions.setBounds(70, 200, 390, 100);
		this.add(predatorOptions);
	}
	
	private void createNightModeOptions() {
		nightModeSwitch = new JPanel();
		JLabel nightModeLabel = new JLabel("Night Mode:");
		nightModeOn = new JRadioButton("On");
		nightModeOff = new JRadioButton("Off");
		
		nightModeLabel.setFont(new Font("Serif", Font.PLAIN, 18));
		nightModeLabel.setForeground(Color.WHITE);
		nightModeOn.setFont(new Font("Serif", Font.PLAIN, 18));
		nightModeOn.setForeground(Color.WHITE);
		nightModeOn.setOpaque(false);
		nightModeOn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isNightMode = true;
			}
		});
		nightModeOff.setFont(new Font("Serif", Font.PLAIN, 18));
		nightModeOff.setForeground(Color.WHITE);
		nightModeOff.setOpaque(false);
		nightModeOff.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isNightMode = false;
			}
		});
		
		ButtonGroup nightModeGroup = new ButtonGroup();
		nightModeGroup.add(nightModeOn);
		nightModeGroup.add(nightModeOff);
		
		nightModeSwitch.add(nightModeLabel);
		nightModeSwitch.add(nightModeOn);
		nightModeSwitch.add(nightModeOff);
		
		nightModeOff.setSelected(true); // default
		
		nightModeSwitch.setOpaque(false);
		nightModeSwitch.setBounds(30, 260, 350, 40);
		
		this.add(nightModeSwitch);
		
	}
	
	
	private void createScreenSizeSlider(){
		screenWidth = 1240;
		screenHeight = 840;
		
		this.screenSize = new JPanel();
		String label0 = "<html><font color = white>Screen Size:<br><font size = 4>Width:<br>Height:</html>";
		JLabel screenSizeLabel = new JLabel(label0);
		
		screenSizeLabel.setFont(new Font("Serif", Font.PLAIN, 18));
		screenSizeLabel.setForeground(Color.WHITE);
		
		screenSize.add(screenSizeLabel);
		
		screenSizeSlider = new JSlider (930,1550,1240);
		screenSizeSlider.setOpaque(false);
		screenSizeSlider.setSnapToTicks(true);
		screenSizeSlider.setMajorTickSpacing(155);
		screenSizeSlider.setPaintTicks(true);
		screenSizeSlider.setPaintLabels(true);
		screenSizeSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (e.getSource() == screenSizeSlider){
					screenWidth = screenSizeSlider.getValue();
					screenHeight = (screenWidth/31)*21+60;
				}		
			}
		});
		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		String label1 = "<html><font color = white>930<br>680</html>";
		String label2 = "<html><font color = white>1085<br>795</html>";
		String label3 = "<html><font color = white>1240<br>900</html>";
		String label4 = "<html><font color = white>1395<br>1065</html>";
		String label5 = "<html><font color = white>1550<br>1110</html>";
		labelTable.put(new Integer(930), new JLabel(label1));
		labelTable.put(new Integer(1085), new JLabel(label2));
		labelTable.put(new Integer(1240), new JLabel(label3));
		labelTable.put(new Integer(1395), new JLabel(label4));
		labelTable.put(new Integer(1550), new JLabel(label5));
		
		screenSizeSlider.setLabelTable(labelTable);
		screenSizeSlider.setPaintLabels(true);
		
		screenSize.add(screenSizeSlider);
		screenSize.setOpaque(false);
		screenSize.setBounds(70, 310, 360, 100);
		
		this.add(screenSize);
		
	}
	
	private void createSaveAndCancel() {
		ImageIcon saveIcon = new ImageIcon("player2down.png");
		mainMenuUpdate = new JButton("Save", saveIcon);
		mainMenuUpdate.setOpaque(false);
		mainMenuUpdate.setContentAreaFilled(false);
		mainMenuUpdate.setBorderPainted(false);
		mainMenuUpdate.setFocusPainted(false);
		//mainMenuUpdate = new JButton("Save");
		mainMenuUpdate.setFont(new Font("Serif", Font.PLAIN, 18));
		mainMenuUpdate.setForeground(Color.WHITE);
		mainMenuUpdate.setBounds(100, 400, 200, 40);
		this.add(mainMenuUpdate);
		ImageIcon cancelIcon = new ImageIcon("player2up.png");
		mainMenu = new JButton("Cancel", cancelIcon);
		mainMenu.setOpaque(false);
		mainMenu.setContentAreaFilled(false);
		mainMenu.setBorderPainted(false);
		mainMenu.setFocusPainted(false);
		//mainMenu = new JButton("Cancel");
		mainMenu.setFont(new Font("Serif", Font.PLAIN, 18));
		mainMenu.setForeground(Color.WHITE);
		mainMenu.setBounds(220, 400, 200, 40);
		this.add(mainMenu);
		
		
	}
	
	@Override
	protected void paintComponent(Graphics g) {
	   super.paintComponent(g); 
	   if (image != null)
	     g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), this);
	}
	
	public void addActionListener (GUI gui) {
		mainMenu.addActionListener(gui);
		mainMenuUpdate.addActionListener(gui);
	}
	
	public int getNumPlayers() {
		return numPlayers;
	}
	
	public int getMazeComplexity() {
		return mazeComplexity;
	}
	
	public int getPredatorStatus() {
		return predatorStatus;
	}
	
	public boolean isNightMode() {
		return isNightMode;
	}
	
	public int getScreenWidth(){
		return screenWidth;
	}
	
	public int getScreenHeight(){
		return screenHeight;
	}
	
	
	public void setMazeComplexity(int mazeComplexity) {
		this.mazeComplexity = mazeComplexity;
		if(mazeComplexity == 1) simple.setSelected(true);
		else complex.setSelected(true);
	}
	
	public void predatorStatusUp() {
		if(this.predatorStatus >= 4) {
			predatorStatus = 4;
			predatorStatusSlider.setValue(4);
		}
		else {
			this.predatorStatus++;
			predatorStatusSlider.setValue(this.predatorStatus);
		}
	}
	
	public void turnOnNightMode(boolean isNightMode) {
		this.isNightMode = isNightMode;
		if(isNightMode) nightModeOn.setSelected(true);
		else nightModeOff.setSelected(true);
	}
	
}



