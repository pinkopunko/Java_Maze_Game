import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * 
 */

/**
 * @author zaisi
 *
 */
public class Board extends JPanel{
	private final int MazeHeight = 21;
	private final int MazeWidth = 31;
	
	BufferedImage wall;
	BufferedImage path;
	BufferedImage wallDark;
	BufferedImage pathDark;
	BufferedImage goal;
	BufferedImage player1left;
	BufferedImage player1right;
	BufferedImage player1up;
	BufferedImage player1down;
	BufferedImage player2left;
	BufferedImage player2right;
	BufferedImage player2up;
	BufferedImage player2down;
	BufferedImage predatorleft;
	BufferedImage predatorright;
	BufferedImage predatorup;
	BufferedImage predatordown;
	BufferedImage black;
	
	private GUI gui;
	private Maze maze;
	private Player player1;
	private Player player2;
	private Predator predator;
	private int numPlayers; // 1 or 2
	private int mazeComplexity; // 1 for DFS; 2 for Prim's
	private int predatorStatus; // 0 for no predator; 1-4 with increasing predator speed
	private boolean isNightMode; // true for night mode; false for day mode
	
	private JLabel instructionBar;
	private JLabel statusBar;
	private Timer timer;
	private int timerCounter; // increments by 4 every second
	
	private int cellWidth() { return (int) this.getSize().getWidth() / MazeWidth; }
    private int cellHeight() { return (int) this.getSize().getHeight() / MazeHeight; }
	
	public Board(GUI gui) {
		this.gui = gui;
		this.numPlayers = 1;
		this.mazeComplexity = 1;
		this.predatorStatus = 0;
		this.isNightMode = false;
		initBoard();	
	}
	
	public void initBoard() {
		setFocusable(true);
		maze = new Maze(MazeWidth, MazeHeight, mazeComplexity);
		player1 = new Player(maze, 1, 0, 1);
		addKeyListener(player1);
		if (numPlayers == 2) {
			player2 = new Player(maze, 2, 0, 1);
			addKeyListener(player2);
		} else player2 = null;
		
		if (predatorStatus != 0) predator = new Predator(maze, MazeWidth - 2, MazeHeight - 2, player1, player2);
		else predator = null;
		
		try {
			wall = ImageIO.read(new File("wall.png"));
			path = ImageIO.read(new File("path.png"));
			goal = ImageIO.read(new File("goal.png"));
			player1left = ImageIO.read(new File("player1left.png"));
			player1right = ImageIO.read(new File("player1right.png"));
			player1up = ImageIO.read(new File("player1up.png"));
			player1down = ImageIO.read(new File("player1down.png"));
			if (numPlayers == 2) {
				player2left = ImageIO.read(new File("player2left.png"));
				player2right = ImageIO.read(new File("player2right.png"));
				player2up = ImageIO.read(new File("player2up.png"));
				player2down = ImageIO.read(new File("player2down.png"));
			}
			if (predatorStatus != 0) {
				predatorleft = ImageIO.read(new File("predatorleft.png"));
				predatorright = ImageIO.read(new File("predatorright.png"));
				predatorup = ImageIO.read(new File("predatorup.png"));
				predatordown = ImageIO.read(new File("predatordown.png"));
			}		
			if (isNightMode) {
				black = ImageIO.read(new File("black.png"));
				wallDark = ImageIO.read(new File("wallDark.png"));
				pathDark = ImageIO.read(new File("pathDark.png"));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.addComponentListener( new ComponentAdapter() {
	        @Override
	        public void componentShown( ComponentEvent e ) {
	            Board.this.requestFocusInWindow();
	        }
	    });
		
		this.setLayout(new BorderLayout());
		if (instructionBar != null) this.remove(instructionBar);
		if (statusBar != null) this.remove(statusBar);
		if (timer != null) timer.stop();
		timerCounter = 0;
		createInstructionBar();
		createStatusBar();
		timer.start();
	}
	
	private void createInstructionBar() {
		instructionBar = new JLabel("");
		instructionBar.setPreferredSize(new Dimension(-1, 40));
		this.add(instructionBar,BorderLayout.NORTH);
		if (numPlayers == 2)
			instructionBar.setText("Puppy: arrow keys   Kittten: WASD");
		else 
			instructionBar.setText("Control your character with arrow keys");
		instructionBar.setFont(new Font("Serif", Font.PLAIN, 18));
		instructionBar.setHorizontalAlignment(SwingConstants.CENTER);
		instructionBar.setForeground(Color.WHITE);
	}
	
	private void createStatusBar() {
		statusBar = new JLabel("");
		statusBar.setPreferredSize(new Dimension(-1, 40));
		this.add(statusBar,BorderLayout.SOUTH);
		
		timer = new Timer(250, new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	timerCounter++;
		    	int hr = 0;
		    	int min = 0;
		    	int sec = timerCounter/4;
		    	while (sec >= 3600) {
		    		hr++;
		    		sec-=3600;
		    	}
		    	while (sec >= 60) {
		    		min++;
		    		sec-=60;
		    	}
		        statusBar.setText("Time elapsed: "+hr+" hr "+min+" min "+sec+" sec   ");
		        statusBar.setFont(new Font("Serif", Font.PLAIN, 18));
		        statusBar.setHorizontalAlignment(SwingConstants.RIGHT);
		        statusBar.setForeground(Color.WHITE);
		        
		        if(predatorStatus != 0 && predator != null) {
		        	if(timerCounter % (5 - predatorStatus) == 0) {
		        		predator.move();
				        if(!player1.captured() && player1.getLocation() == predator.getLocation()) {
				        	removeKeyListener(player1);
				        	player1.getLocation().setPlayer(1, false);
				        	player1.setCaptured(true);
				        }
				        if(player2 != null && !player2.captured() && player2.getLocation() == predator.getLocation()) {
				        	removeKeyListener(player2);
				        	player2.getLocation().setPlayer(2, false);
				        	player2.setCaptured(true);
				        }
				        if((player1.captured() == true && player2 == null) ||
				           (player1.captured() == true && player2 != null && player2.captured())){
				        	timer.stop();
				        	gui.showDefeatScreen();
				        }
			        }     
		        }
		    }
		});
	}
	
	
	@Override
    public void paintComponent(Graphics g) {
		repaint();
        super.paintComponent(g);
        doDrawing(g);
    }
	
	
    
	private void doDrawing(Graphics g) {
       
        for (int j = 0; j < MazeHeight; j++) {
			for (int i = 0; i < MazeWidth; i++) {
				Cell cell = maze.getCell(i, j);
				int drawx = i * cellWidth();
				int drawy = j * cellHeight();
				
				if (isNightMode){
					//set the distance from the closest source of light
					this.setDistFromLight(cell, i, j);
					
					// if our cell is far to a source of light
					if (cell.getDistFromLight() > 3){
						g.drawImage(black, drawx, drawy,
								drawx+this.cellWidth(),drawy+this.cellHeight(),
								0,0,40,40, null);
						continue;
						
					// else if our cell is 3 cells from a source of light
					} else if (cell.getDistFromLight() == 3){
						// if it's a wall show "dark wall"
						if (cell.isWall()) g.drawImage(wallDark, drawx, drawy,
								drawx+this.cellWidth(),drawy+this.cellHeight(),
								0,0,40,40, null);
						// if it's a path, show "dark path"
						else g.drawImage(pathDark, drawx, drawy,
								drawx+this.cellWidth(),drawy+this.cellHeight(),
								0,0,40,40, null);
						continue;
					}
				}
				
				
				if (cell.isWall()) g.drawImage(wall, drawx, drawy,
						drawx+this.cellWidth(),drawy+this.cellHeight(),
						0,0,40,40, null);
				else if (cell.isGoal()) g.drawImage(goal, drawx, drawy,
						drawx+this.cellWidth(),drawy+this.cellHeight(),
						0,0,40,40, null);
				else g.drawImage(path, drawx, drawy,
						drawx+this.cellWidth(),drawy+this.cellHeight(),
						0,0,40,40, null);
				if (!player1.captured() && cell.player(1)) {
					String player1Facing = player1.getFacing();
					if (player1Facing.equals("up"))	g.drawImage(player1up, drawx, drawy,
							drawx+this.cellWidth(),drawy+this.cellHeight(),
							0,0,40,40, null);
					else if (player1Facing.equals("down"))	g.drawImage(player1down, drawx, drawy,
							drawx+this.cellWidth(),drawy+this.cellHeight(),
							0,0,40,40, null);
					else if (player1Facing.equals("left"))	g.drawImage(player1left, drawx, drawy,
							drawx+this.cellWidth(),drawy+this.cellHeight(),
							0,0,40,40, null);
					else if (player1Facing.equals("right"))	g.drawImage(player1right, drawx, drawy,
							drawx+this.cellWidth(),drawy+this.cellHeight(),
							0,0,40,40, null);
				}
				if (player2 != null && !player2.captured() && cell.player(2)) {
					String player2Facing = player2.getFacing();
					if (player2Facing.equals("up"))	g.drawImage(player2up, drawx, drawy,
							drawx+this.cellWidth(),drawy+this.cellHeight(),
							0,0,40,40, null);
					else if (player2Facing.equals("down"))	g.drawImage(player2down, drawx, drawy,
							drawx+this.cellWidth(),drawy+this.cellHeight(),
							0,0,40,40, null);
					else if (player2Facing.equals("left"))	g.drawImage(player2left, drawx, drawy,
							drawx+this.cellWidth(),drawy+this.cellHeight(),
							0,0,40,40, null);
					else if (player2Facing.equals("right"))	g.drawImage(player2right, drawx, drawy,
							drawx+this.cellWidth(),drawy+this.cellHeight(),
							0,0,40,40, null);
				}
				if (predatorStatus != 0) {
					if (cell.predator()) {
						String predatorFacing = predator.getFacing();
						if (predatorFacing.equals("up"))	g.drawImage(predatorup, drawx, drawy,
								drawx+this.cellWidth(),drawy+this.cellHeight(),
								0,0,40,40, null);
						else if (predatorFacing.equals("down"))	g.drawImage(predatordown, drawx, drawy,
								drawx+this.cellWidth(),drawy+this.cellHeight(),
								0,0,40,40, null);
						else if (predatorFacing.equals("left"))	g.drawImage(predatorleft, drawx, drawy,
								drawx+this.cellWidth(),drawy+this.cellHeight(),
								0,0,40,40, null);
						else if (predatorFacing.equals("right")) g.drawImage(predatorright, drawx, drawy,
								drawx+this.cellWidth(),drawy+this.cellHeight(),
								0,0,40,40, null);
					}
				}
			}
		}
	}
	
	public void setDistFromLight(Cell cell, int i, int j){
		
		int p1x = 100; // player1 coordinates
		int p1y = 100;
		int p2x = 100; // player2 coordinates
		int p2y = 100;
		int pdx = 100; // predator coordinates
		int pdy = 100;
		int gx = MazeWidth - 2; // goal coordinates
		int gy = MazeHeight - 2;
		if (!player1.captured()) {
			p1x = this.player1.getLocation().getX();
			p1y = this.player1.getLocation().getY();
		}
		if (player2 != null && !player2.captured()){
			p2x = this.player2.getLocation().getX();
			p2y = this.player2.getLocation().getY();
		}
		if (predatorStatus != 0) {
			pdx = this.predator.getLocation().getX();
			pdy = this.predator.getLocation().getY();
		}
		// else apply pythagoras and set the distance from the closest source of light
		int d = 90;
		int dg = this.pythagoras(i, j, gx, gy);
		if (dg < d) d = dg;
		if (!player1.captured()){
			int d1 = this.pythagoras(i, j, p1x, p1y);
			if (d1 < d) d = d1;
		}
		if (player2 != null && !player2.captured()){
			int d2 = this.pythagoras(i, j, p2x, p2y);
			if (d2 < d) d = d2;
		}
		if (predatorStatus != 0) {
			int dp = this.pythagoras(i, j, pdx, pdy);
			if (dp < d) d = dp;
		}
		cell.setDistFromLight(d);
	}
	
	// pythagoras takes in 2 coordinates and returns the distance
	private int pythagoras(int x1, int y1, int x2, int y2){
		return (int) Math.sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
	}
	
	public void setNumPlayers(int numPlayers) {
		this.numPlayers = numPlayers;
	}
	
	public void setMazeComplexity(int mazeComplexity) {
		this.mazeComplexity = mazeComplexity;
	}
	
	public void setPredatorStatus(int predatorStatus) {
		this.predatorStatus = predatorStatus;
	}
	
	public void setNightMode(boolean isNightMode) {
		this.isNightMode = isNightMode;
	}
	
	public void stopTimer() {
		timer.stop();
		return;
	}
	
	public Maze getMaze() {
		return maze;
	}
	
	public Player getPlayer(int ref) {
		if (ref == 1) return player1;
		else if (ref == 2) return player2;
		else return null;
	}
	
	public int getNumPlayers() {
		return numPlayers;
	}
	
	public int getMazeComplexity() {
		return mazeComplexity;
	}
	
	public int getPredatorStatus() {
		return predatorStatus;
	}
	
	public boolean isNightMode() {
		return isNightMode;
	}
	
	public int getSecondCounter() {
		return timerCounter/4;
	}
}
